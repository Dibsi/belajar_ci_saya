<?php 
    echo "Muncul Angka Genap : " ;
    # Clue : Karena syarat bilangan genap yaitu jika bilangan itu di bagi 2 sisa 0. 
    for ($x=1; $x <= 10; $x++){ // Mengulang selama 10 kali yang di mulai dari angka 1
      if ( $x % 2 == 0 ){ // Mengecek apakah bilangan itu dibagi 2 sisa 0 
        echo " ".$x; // Jika benar bilangan itu di bagi 2 sisa 0 maka menampilkan bilangan itu, yang dipisahkan oleh string kosong 
      }
    }
    # Hasil : 2 4 6 8 10